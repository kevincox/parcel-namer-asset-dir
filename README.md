# parcel-namer-asset-dir

**WARNING: This is currently using a prefix instead of a subdirectory due to a bug in parcel.  https://github.com/parcel-bundler/parcel/issues/4172. When that bug is fixed the prefix will be switched from `asset-dir-prefix-` to `a/` with only a minor version update.**

A namer plugin for https://parceljs.org.

A parcel namer that puts all of your assets into a subdirectory.

- Entry points are left to the default namer.
- Assets are named `a/{slug}.{hash}.{ext}`.

This makes it easy to do a number of things:

- Apply aggressive caching to everything inside `a/`.
- Rewrite anything outside of `a/` to `index.html` for a single page app.
- Upload everything inside of `a/` first, then upload everything else to avoid version-skew issues.

There is currently no configuration.

## Usage

```
$ npm install --save-dev parcel-namer-asset-dir
```

```json
// .parcelrc
{
	"extends": ["@parcel/config-default"],
	"namers": [
		"parcel-namer-asset-dir",
		"@parcel/namer-default",
	],
}
```
