const {Namer} = require('@parcel/plugin');
const path = require('path');

const COMMON_NAMES = {
	index: true,
	lib: true,
};

/// Return a slug to give a hint what the file is.
//
// This shouldn't be required for uniqueness since the assets include a hash in the path however it is convenient for humans looking at stack traces and network monitors.
//
// Possible improvements:
// - Allow omitting this from the file path to save some bytes (and buy some obfuscation).
// - Allow including more of the path (for example "some-package-type" instead of just "type").
//
// However these would require some user config.
function nameHint(p) {
	let parsed = path.parse(p);
	while (COMMON_NAMES[parsed.name]) {
		parsed = path.parse(parsed.dir);
	}
	return parsed.name;
}

module.exports = new Namer({
	async name({bundle}) {
		if (bundle.needsStableName) {
			return null;
		}

		let name = "";
		let mainEntry = bundle.getMainEntry();
		if (mainEntry.filePath) {
			name = nameHint(mainEntry.filePath) + ".";
		}
		
		// https://github.com/parcel-bundler/parcel/issues/4172
		// Currently having files in different directories is broken. So we are
		// using a prefix for now.
		return `asset-dir-prefix-${name}${bundle.hashReference}.${bundle.type}`;
	},
});
